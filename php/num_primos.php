<html>
<head>
    <title>Numeros primos</title>
</head>
<body>
<form action="#" method="post">
<input type="text" name="desde">
<input type="text" name="hasta">
<input type="submit" value="Calcular">


</form>

<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE); //Esto evita que aparezcan E_WARNING,E_ERROR,E_PARSE
$n1 = $_POST['desde']; // Desde
$n2 = $_POST['hasta']; // Hasta
if ($n1 != "" || $n2 != "") { //esto evita que aparezcan mensajes sin haber introducido datos
    print 'Números primos del ';
    print $n1;
    print ' al ';
    print $n2;
    for ($i = $n1; $i <= $n2; $i++) { // aquí se define el intervalo que contendrá los números a evaluar
        $nDiv = 0; // Número de divisores
        for ($n = 1; $n <= $i; $n++) // Desde 1 hasta el valor que tenga $i
        {
            if ($i % $n == 0) // $n es un divisor de $i
            {
                $nDiv = $nDiv + 1; // Agregamos un divisor mas.
            }
        }
        if ($nDiv == 2 or $i == 1) // Si tiene 2 divisores ó es 1 --> Es primo
        {
            print '<br>';
            print $i;
        }
    }
}
?>
</body>
</html>