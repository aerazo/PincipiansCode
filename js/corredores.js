//De cuatro corredores de atletismo se sabe que C ha llegado inmediatamente despues de B, D ha llegado enmedio de A y C. ¿Podrías calcular el orden de llegada?

var a = {
    A: 0,
    B: 0,
    C: 0,
    D: 0,

    resultado: function() {

        if (a.C > a.B && // B llega primero que C
            a.D > a.B && // B llegó primero que D
            a.D > a.C && // C llega primero que D
            a.D < a.A) { // A llega mas tarde que D
            return true;

        }
        return false;
    },

    intervalo: setInterval(function() {
        a.A = Math.ceil(Math.random() * 4); // asignan valores aleatorios a los corredores
        a.B = Math.ceil(Math.random() * 4);
        a.C = Math.ceil(Math.random() * 4);
        a.D = Math.ceil(Math.random() * 4);

        if (a.resultado()) {
            clearInterval(a.intervalo); // se limpia el intervalo
            console.log("Atleta A", a.A); // se muestra los datos en consola
            console.log("Atleta B", a.B);
            console.log("Atleta C", a.C);
            console.log("Atleta D", a.D);

        }


    }, 10)
};